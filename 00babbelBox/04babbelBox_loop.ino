uint16_t idleClocks = 0;

void loop() {
  continueAfterStart();

  // Serial.println(F("read NFC"));
  if (trackChangeOnRfidToken()) {
    if (!musicPlayer.stopped()) {
      musicPlayer.stopPlaying();
      queueRewind();
      ampOn(false);
    }
    #if COMMIT_SOUND
    playTone(0x43, 25);
    #endif
    // TODO: alternate append queue

    queuePlay(playnuid.c_str());
    idleClocks = 0;
    delay(100);
  }

  // Serial.println(F("play next if stopped"));
  playNextIfStopped();

  if (musicPlayer.stopped()) { // TODO: prevent from entering on queue track change
    if (idleClocks == 0) {
      musicPlayer.setVolume(INITIAL_VOLUME, INITIAL_VOLUME);
      volume = INITIAL_VOLUME;
#if SERIAL_DEBUG_VERBOSE
      Serial.println(F("Done playing music"));
#endif
      playToneSequence(0x43, 0x42, 0x41, 25);
    }
    // Nag user if device is idling ... yes, he can shut down
    idleClocks++;
    if (idleClocks % 1000 == 0) {
      idleClocks = 1; // reset to prevent buffer overflow
      playToneSequence(0x43, 0x42, 0x41, 25);
    }
    // TODO: get more nervous and naggy by time
    delay(100);
    return;
  }

  // if we get an 's' on the serial console, stop!
  if (!digitalRead(BUTTON_STOP)) {
    if (!buttonReleased) return;
    buttonReleased = false;
    fullStop();
  }

  // if we get an 'p' on the serial console, pause/unpause!
  else if (!digitalRead(BUTTON_PLAY_PAUSE)) {
    if (!buttonReleased) return;
    buttonReleased = false;
    if (! musicPlayer.paused()) {
      Serial.println(F("Paused"));
      musicPlayer.pausePlaying(true);
      ampOn(true);
    } else {
      Serial.println(F("Resumed"));
      musicPlayer.pausePlaying(false);
      ampOn(true);
    }
    #ifdef COMMIT_BUTTON_SOUND
    playTone(0x43, 25);
    #endif
    delay(200);
  }

  else if (!digitalRead(BUTTON_SOFTER) || !digitalRead(BUTTON_LOUDER)) {
    if (! musicPlayer.paused()) {
      buttonReleased = false;
      // TODO: increase logarithmic because loudness is perceived that way
      int newVol = volume + (10 * (!digitalRead(BUTTON_SOFTER) ? 1 : -1));
      // reset on both buttons are pressed
      if (!digitalRead(BUTTON_SOFTER) && !digitalRead(BUTTON_LOUDER)) newVol = INITIAL_VOLUME;
      if (newVol < 0 || newVol > 255) return; // TODO: play tone indicator
      Serial.println(!digitalRead(BUTTON_SOFTER) ? F("piano") : F("louder"));
      volume = newVol;
      Serial.println(volume);
      musicPlayer.setVolume(volume, volume);
      // TODO: play a tone indicator musicPlayer.sineTest(0x44, 25); // sineTest ends in endless beep
      delay(200);
    } else {
      if (!buttonReleased) return;
      buttonReleased = false;
      
      musicPlayer.stopPlaying();
      ampOn(false);

      if (!digitalRead(BUTTON_SOFTER)) {
        nextFileInQueue--;
        if (nextFileInQueue > 0) {
          playToneSequence(0x42, 0x41, 25);
          nextFileInQueue--;
        } else {
          playToneSequence(0x41, 0x41, 25);
        }
      } else if (!digitalRead(BUTTON_LOUDER)) {
        playToneSequence(0x42, 0x43, 25);
      }
      delay(200);
    }
  }

#if false
  else if (Serial.available() && 'c' == Serial.read()) {
    Serial.println(F(musicPlayer.decodeTime()));
    musicPlayer.dumpRegs();
  }

  // NOT WORKING
  else if (Serial.available() && 'f' == Serial.read()) {
    uint16_t ctime = musicPlayer.decodeTime();
    Serial.println(F(ctime));
    ctime += 10;
    noInterrupts();
    // musicPlayer.pausePlaying(true);
    musicPlayer.sciWrite(VS1053_REG_DECODETIME, ctime);
    musicPlayer.sciWrite(VS1053_REG_DECODETIME, ctime);
    //  musicPlayer.pausePlaying(false);
    musicPlayer.feedBuffer();
    Serial.println(F(ctime));
  }
#endif

  else {
    // give user time to release keys
    if (!buttonReleased) delay(400);
    buttonReleased = true;
  }

  // indeed needed, else some mp4/aac files will stuck
  if (musicPlayer.playingMusic) {
    musicPlayer.feedBuffer();
  }
}
