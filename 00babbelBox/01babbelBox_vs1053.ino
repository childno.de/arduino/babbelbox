#include <Adafruit_VS1053.h>

// These are the pins used for the breakout example
#define PIN_VS1053_RESET  9      // VS1053 reset pin (output)
#define PIN_VS1053_CS     7      // default 10 // VS1053 chip select pin (output)
#define PIN_VS1053_DCS    8      // VS1053 Data/command select pin (output)

// PIN_DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define PIN_DREQ          3      // VS1053 Data request, ideally an Interrupt pin

const int INITIAL_VOLUME = 60;

int volume = INITIAL_VOLUME;

Adafruit_VS1053_FilePlayer musicPlayer =
  // create breakout-example object!
  Adafruit_VS1053_FilePlayer(PIN_VS1053_RESET, PIN_VS1053_CS, PIN_VS1053_DCS, PIN_DREQ, PIN_SD_CS);

void setupVs1053Board() {
  musicPlayer.setVolume(INITIAL_VOLUME, INITIAL_VOLUME);

  // Timer interrupts are not suggested, better to use PIN_DREQ interrupt!
  //musicPlayer.useInterrupt(VS1053_FILEPLAYER_TIMER0_INT); // timer int

  // If PIN_DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  if (! musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT))
    Serial.println(F("DREQ pin is not an interrupt pin"));
}

/***************************************************
  Based on an example for the Adafruit VS1053 Codec Breakout

  Designed specifically to work with the Adafruit VS1053 Codec Breakout
  ----> https://www.adafruit.com/products/1381

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/
void checkVs1053Board() {
#if SERIAL_DEBUG_VERBOSE
  Serial.println(F("== Adafruit VS1053 self test"));
#endif

  if (! musicPlayer.begin()) { // initialise the music player
#if SERIAL_DEBUG
    Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
#else
    // TODO playToneSequence(0x47, 0x48, 0x46, 100);
#endif
    while (1);
  }

#if SERIAL_DEBUG_VERBOSE
  Serial.println(F("VS1053 found"));
#endif
}
