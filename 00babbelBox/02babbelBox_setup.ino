void setup() {
  setupAmp();
  ampOn(false);

#if SERIAL_DEBUG
  Serial.begin(9600);   // Initialize serial communications with the PC
#if SERIAL_DEBUG_VERBOSE
  Serial.println(F("== BABBELBOX 0.2"));
#endif
#endif

  checkVs1053Board();
  if (!checkMifareBoard()) {
    // TODO: use piezo to indicat problem
    playToneSequence(0x48, 0x46, 0x47, 100);
  }
  checkSdCard();
  setupVs1053Board();
  setupMifareBoard();
  setupButtons();

  // all finished, indicate success
  playToneSequence(0x44, 0x45, 0x46, 25);

#if SERIAL_DEBUG_VERBOSE && false
  // list files
  printDirectory(SD.open("/"));
#endif
}
