String playnuid = "";

bool trackChangeOnRfidToken() {
  String detectednuid = readMifareUid();
#if SERIAL_DEBUG_VERBOSE
  if (detectednuid != "" || playnuid != "") {
    Serial.print("\ndetected nuid:");
    Serial.print(detectednuid);
    Serial.print("\nplay nuid:");
    Serial.print(playnuid);
  }
#endif
  if (!playnuid.equals(detectednuid) && detectednuid.length() > 0) {
    playnuid = String(detectednuid.c_str());
#if SERIAL_DEBUG
    Serial.print("\nset detected Tag for play: ");
    Serial.print(playnuid);
#endif
    return true;
  }
  return false;
}
