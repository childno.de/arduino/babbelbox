#define PIN_AMP_SHUTDOWN  5

void setupAmp() {
  pinMode(PIN_AMP_SHUTDOWN, INPUT);
}

void ampOn(bool yes) {
  digitalWrite(PIN_AMP_SHUTDOWN, static_cast<uint8_t>(yes ? HIGH : LOW));
}
