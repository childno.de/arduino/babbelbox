= Was das hier tut
Ein Musik und Hörspiel-Abspielgerät

== Meine Hardware
… 🛠 siehe [00babbelBox/00babbelBox.ino]

== Meine Anforderungen
… 🛠 siehe [00babbelBox/00babbelBox.ino]

== Meine Wünsche (man muss ja Ziele haben)
… 🛠 siehe [00babbelBox/00babbelBox.ino]

= Wat, dat gibt es schon?! ;)
== ander Nachbauten
* https://daddydoes.de/2018/02/14/wheezybox-alternative-toniebox/

== Professionelle Produkte
== boxine® tonies®
allen vorran sei boxine® (https://boxine.de) mit Ihren tonies® samt Toniebox (https://tonies.de) genannt.

GROßARTIGES Produkt. Fullstop.
Wenn man nicht gerade Hacker / Maker ist und alles selbst machen will. 

✔︎ ︎Produkt aus Deutschland
✔︎ ︎Materialien heißt es sind mitunter Nachhaltig (Stoff)
? Zu Plaste wird nix gesagt
✔ ︎Hochwertiges, schönes, Kindgerechted Produkt
‼️ Inhalte werden stets nur über die Cloud freigebeben
‼️ Keine Abspielmöglichkeiten anderer Medien / Dateien / Audioeingänge

Kinderzimmertauglichkeit
✔  Kein Mikrofon verbaut

Die Figuren mit denen man Inhalte erwirbt gibt es "Vorbespielt" mit inhalten und als
90m "Leerkassetten"
(i.S. rechtlich vermutlich eher so, dass man die Figur erwirbt + eine Lizenz für die Inhalte - die jederzeit auch widerrufen werden kann -> vgl. Musik Streaming anbieter)
‼ um "Kreativ-Tonies" aka Leerkassetten zu befüllen, braucht man ein Smartphone + Tonie App oder einen Computer und Aufgenommene Audio Dateien.

Die Jungs und Mädels geben sich auch echt Mühe gute und verständliche Einführungsvideos (YouTube https://www.youtube.com/channel/UCc8O-j5IKJSQ5OrTRnV4mFA/videos)
sowie die Technik so auszulegen, dass Fehler leicht erkannt werden können (Ausgesprochene Tiernamen statt cryptischer Fehlerpiepser).

Kreativ-Tonies haben auch Funktionen, die Jetlag-Daddies und -Mamas mögen, damit Sie Ihren Kindern die Sie nie sehen eine Sprachnachricht zukommen lassen können.

Genug der Lobhuddelei ;) Kaufen!

Wen die Technik im inneren interessiert:
https://de.ifixit.com/Teardown/Toniebox+Teardown/106148


Preislage:
Anschaffung 💶💶
Unterhalt   💶💶💶


=== Toniebox zerlegung
* https://medium.com/@paul.klingelhuber/first-quick-security-impressions-of-the-toniebox-d609656634ac

== Hörbert
der Hörbert (https://www.hoerbert.com) ist ein anderer toller, nachhaltiger MP3 Player aus Deutschland und hiesiger Produktion.
Wobei ich auch sagen muss, dass es sich mit der Nachhaltigkeit dann auch hat, wenn man bedenkt, dass kein Netzteil dabei ist und
immer auf Batteriebetrieb läuft. Akkus und Ladegerät... muss man selbst besorgen.. Das käme auf den Kaufpreis dann nochmal drauf
plus den Aufwand Batterien zu wechseln.

Kinderzimmertauglichkeit:
‼️ Tiefentladeschutz gibt es nur optional! (https://www.hoerbert.com/kaufen/abschalt-automatik-fuer-hoerbert/)
✔ ︎kein Mikrofon verbaut
‼️ Potentiometer als Lautstärkeregler halte ich persönlich auch nicht für Kleinkindgeeignet 

kann ich aber nicht viel zu sagen, außer dass ich das Konzept begrenzter Inhalte auf dem Gerät nicht ganz verstehe weil ich sehr schnell sehe,
dass Kinder "Nachschlag" fordern und man damit immer wieder beschäftigt ist Inhalte auszutauschen..
auf der anderen Seite: Es gibt eben auch nicht alle ritt was neues. Auch das können Kinder gut lernen ;)

Preislage:
Anschaffung 💶💶💶
Unterhalt   💶

== alilo® Hunny Bunny
alilo®'s (https://alilo.de) Hunny Bunny und Co. bieten angeblich auch guten Klang. Wenig verwunderlich bei dem großen Holraum alias Kopf.

Herkunft, Herstellung, Nachhaltigkeit, Schadstoffbelastung ... unbekannt.


Preislage:
Anschaffung 💶💶
Unterhalt   💶 (Inhalte können beliebig besorgt werden)

== Tiger Media tigerbox

die tigerbox von Tiger Media (https://tiger.media/tigerbox/) hat eine deutlich Kompaktere Bauform
… und wenn man genauer liest, auch einen deutlich anderen "Lieferumfang" der den "viel günstigeren Preis" erklärt

✔︎ ︎billisch
‼ ist lediglich ein Bluetooth Lautsprecher mit SD Karten Slot
✘ braucht eine SD Karte um Offline Musik zu spielen -> muss noch zusätzlich gekauft werden!
‼ Hörbücher von Tiger Media's tigertones (https://tiger.media/tigertones/) werden nur per Stream via Smartphone bereit gestellt -> Ohne Smartphone, keine Musik

Bezüglich Kinderzimmertauglichkeit
‼ hat ein Mikrofon verbaut

Wenig für Kleinkinder geeignet, da die Knöpfe auch verstanden sein müssen … aber gut, Kinder lernen alles und schneller als man will ;p


Preislage:
Anschaffung 💶
Unterhalt   💶(💶 Bequem ist es sicher über die tigertones, preislich ist man woanders mit MP3 download vermutlich aber günstiger)

= Für und gegen RasPi
== Boot Time
ja, da gibt es [Dinge](http://himeshp.blogspot.com/2018/08/fast-boot-with-raspberry-pi.html) aber ernsthaft ... SD & CPU übertakten? Das Ding soll stabil laufen!

= Ideas / Inspirationen
== Aussehen
* Card Reader Style https://mobile.twitter.com/ChrisJPatty/status/1078065910487760896

= Technisch:
== Boards

https://store.arduino.cc/nano-33-iot        SAMD21G18A  <= 48MHz    256KB Flash      32KB SRAM
https://store.arduino.cc/nano-33-ble        nRF 52840   64 Mhz      1MB Flash       256KB RAM
https://store.arduino.cc/nano-33-ble-sense  nRF 52840   64 Mhz      1MB Flash       256KB RAM

Adafruit Feather M0 Bluefruit LE (30 $USD)
https://www.adafruit.com/product/2995       ATSAMD21G18 48 MHz      256KB Flash      32KB RAM

Adafruit Feather 32u4 Bluefruit
https://www.adafruit.com/product/2829       ATmega32u4   8 MHz       32KB Flash       2KB RAM

Adafruit Feather nRF52840 Express (25 $USD)
https://www.adafruit.com/product/4062       nRF 52840   64 Mhz      1MB Flash       256KB RAM
(Cortex M4F)
+ USB Battery Charge

== Batterie
https://forum.arduino.cc/index.php?topic=418299.0
https://www.open-electronics.org/the-power-of-arduino-this-unknown/

=== QI
https://www.exp-tech.de/zubehoer/sonstige/7724/universal-qi-wireless-receiver-module

== NFC / RFID

=== RC522
https://www.nxp.com/docs/en/data-sheet/MFRC522.pdf

=== PN532
https://www.nxp.com/docs/en/user-guide/141520.pdf

bspw. https://www.seeedstudio.com/Grove-NFC-p-1804.html

* https://github.com/Seeed-Studio/PN532
* https://github.com/don/NDEF