#if false
/// File listing helper
int countFilesIn(File dir) {
  int count = 0;
  dir.rewindDirectory();
  File entry =  dir.openNextFile();
  while (entry) {
    if (!entry.isDirectory()) {
      count++;
    }
    entry.close();
    entry = dir.openNextFile();
  }
  return count;
}
int countAndPrintRecursive(File dir, int numTabs, int count) {
  if (!SD.exists(dir.name())) {
    Serial.print("\n!!!!! dir doesn't exist by name `");
    Serial.print(dir.name());
    Serial.print("`!!!!!");
    return;
  }
  dir.rewindDirectory();
  File entry =  dir.openNextFile();
  Serial.print('\n');
  while (entry) {
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println(F("/"));
      countAndPrintRecursive(entry, numTabs + 1, count);
    } else {
      count++;
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
    entry = dir.openNextFile();
  }
  return count;
}
void printDirectory(File dir) {
  countAndPrintRecursive(dir, 0, 0);
}

#if EXPERIMENTAL
*String listDirectoryRecursive(File dir, File parent, String *list[], int *entrynum) {
  int count = countAndprintDirectory(dir, 0, 0);
  if (list == NULL) {
    list = list[count];
  }
  File entry =  dir.openNextFile();
  while (entry) {
    if (entry.isDirectory()) {
      listDirectoryRecursive(entry, dir, list, entrynum);
    } else {
      list[entrynum] = entry.name();
    }
    entry.close();
    entrynum++;
    entry =  dir.openNextFile();
  }
}
#endif

void listDirectory(File dir, char (*list)[13]) {
#ifdef SERIAL_DEBUG_VERBOSE
  Serial.print("\ndir: ");
  Serial.print(dir.name());
#endif
  int count = sizeof(list) / 13;
  int entrynum = count;
  dir.rewindDirectory();
  File entry =  dir.openNextFile();
  while (entry) {
    if (!entry.isDirectory()) {
#ifdef SERIAL_DEBUG_VERBOSE
      // Serial.print(entrynum - count);
#endif
      // list[entrynum - count] = new String(entry.name());
      strncpy(list[(entrynum - count)], entry.name(), 12);
      list[(entrynum - count)][12] = 0;
      /*
        Serial.print(list[(entrynum - count)]);
        Serial.print("\n");
        Serial.print(sizeof(list));
        Serial.print("\n");
      */
      entrynum++;
    }
    entry.close();
    entry =  dir.openNextFile();
  }
}


void listDirectory(File dir, File *list) {
  Serial.print("dir: ");
  Serial.print(dir.name());
  int count = sizeof(list) / 13;
  int entrynum = count;
  dir.rewindDirectory();
  File entry =  dir.openNextFile();
  while (entry) {
    if (!entry.isDirectory()) {
      list[(entrynum - count)] = entry;
      entrynum++;
    }
    entry.close();
    entry = dir.openNextFile();
  }
}
#endif

void getNextFile(char nextFile[13], char queueDirname[8], int nextFileInQueue) {
  nextFile[0] = '\0';
  File dir = SD.open(queueDirname);
  if (!SD.exists(dir.name()) || !dir.isDirectory()) {
    Serial.print("dir doesn't exist: ");
    Serial.print(dir.name());
    return;
  }
  dir.rewindDirectory();
#if SERIAL_DEBUG_VERBOSE
  Serial.print("\nopened dir: ");
  Serial.print(dir.name());
#endif

#if false
  printDirectory(dir);
#endif
  File entry = dir.openNextFile();
  int i = 0;
  while (entry) {
    if (!entry.isDirectory()) {
#if SERIAL_DEBUG_VERBOSE
      Serial.print("\ni: ");
      Serial.print(i);
      Serial.print(" of expected ");
      Serial.print(nextFileInQueue);
      Serial.print(" : ");
      Serial.print(entry.name());
#endif
      if (i == nextFileInQueue) {
        strncpy(nextFile, entry.name(), 12);
        entry.close();
        nextFile[12] = 0;
#if SERIAL_DEBUG_VERBOSE
        Serial.print("\nfound nextFile is: ");
        Serial.print(String(nextFile));
#endif
        return;
      }
      i++;
    }
    entry.close();
    entry = dir.openNextFile();
  }
#if SERIAL_DEBUG_VERBOSE
  Serial.print("\nnothing found for queue: ");
  Serial.print(dir.name());
#endif
}
