#include <EEPROMex.h>
bool inAutostart = true;
const int memBase = 350;

bool changedState = false;
int lastState = 0;
int newState = 0;

void continueAfterStart() {
  newState = musicPlayer.playingMusic ? 1 : musicPlayer.paused() ? 2 : 0;
  changedState = (lastState != newState);

  int waitMillis = 0;
  int addrQueueName = EEPROM.getAddress(sizeof(char)*8);
  int addrQueueNum = EEPROM.getAddress(sizeof(int));

  while (!EEPROM.isReady()) {
    if (waitMillis < 10000) {
      delay(1);
      waitMillis++;
      // playTone(0x40, 0x25);
      continue;
    }
    playToneSequence(0x40, 0x40, 0x25);
    return;
  }
  EEPROM.setMemPool(memBase, EEPROMSizeUno);
  // EEPROM.setMaxAllowedWrites(maxAllowedWrites);

  if (!changedState && !inAutostart) {
    return;
  }
  if (musicPlayer.stopped()) {
    if (inAutostart) {
      EEPROM.readBlock<char>(addrQueueName, queueDirname, 8);
      Serial.println(F("restored state"));
      nextFileInQueue = EEPROM.readInt(addrQueueNum);
      inAutostart = false;
      return;
    }
  }
  if (musicPlayer.paused()) {
    Serial.println(F("saved state"));
    EEPROM.updateBlock<char>(addrQueueName, queueDirname, 8);
    EEPROM.updateInt(addrQueueNum, (nextFileInQueue - 1));
  }
  else if (lastState == 2 || musicPlayer.stopped() && String(queueDirname).length() == 0) {
    Serial.println(F("clear state"));
    EEPROM.updateBlock<char>(addrQueueName, "", 8);
    EEPROM.updateInt(addrQueueNum, 0);
  }
  else {
    Serial.println(F("ignored state change"));
  }
  lastState = newState;
}
