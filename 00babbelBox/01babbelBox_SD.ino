#include <SD.h>

// These are common pins between breakout and shield
#define PIN_SD_CS         4      // Card chip select pin

void checkSdCard() {

#if SERIAL_DEBUG_VERBOSE
  Serial.println(F("== SDCard Check"));
#endif
  if (!SD.begin(PIN_SD_CS)) {
#if SERIAL_DEBUG
    Serial.println(F("SD failed, or not present"));
#endif
    while (1);  // don't do anything more
  }
}
