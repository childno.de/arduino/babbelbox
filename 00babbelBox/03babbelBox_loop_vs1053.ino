int nextFileInQueue = 0;
char queueDirname[8];

void queueRewind() {
  nextFileInQueue = 0;
}

void fullStop() {
  musicPlayer.stopPlaying();
  queueReset();
  ampOn(false);
  delay(200);
}

void queueReset() {
  playnuid = "";
  queueRewind();
  memset(queueDirname, 0, sizeof(queueDirname));
}

void playNextIfStopped() {
  if (musicPlayer.stopped()) {
    if (String(queueDirname).length() == 0) {
#if SERIAL_DEBUG_VERBOSE
      Serial.println(F("(i) nothing to play in queue"));
#endif
      return;
    }
    char nextFile[13];
    getNextFile(nextFile, queueDirname, nextFileInQueue);
#if SERIAL_DEBUG_VERBOSE
    Serial.print("\nnextFile is: ");
    Serial.print(String(nextFile));
#endif

    if (nextFile[0] != '\0') {
      String playfile = String(queueDirname);
      playfile.concat("/");
      playfile.concat(nextFile);
#if SERIAL_DEBUG_VERBOSE
      Serial.print("\n>>>> PLAYing ");
      Serial.print(playfile.c_str());
#endif
      musicPlayer.startPlayingFile(playfile.c_str());
      musicPlayer.setVolume(volume, volume);
      ampOn(true);
      nextFileInQueue++;
      delay(100);
      return;
    }
#if SERIAL_DEBUG
    Serial.println(F("(i) queue finished"));
#endif
    fullStop();
    delay(100);
  }
}

void queuePlay(char * dirname) {
  nextFileInQueue = 0;
#if SERIAL_DEBUG_VERBOSE
  Serial.print(F("queue to play `"));
  Serial.print(String(dirname));
  Serial.println(F("`"));
#endif
  if (String(dirname).length() == 0) {
    Serial.println(F("!! queuePlay no dir name given"));
    return;
  }
  if (!SD.exists(dirname)) {
    SD.mkdir(dirname);
    playToneSequence(0x42, 0x42, 0x42, 25);
  }
  strcpy(queueDirname, dirname);
  
#if SERIAL_DEBUG
  Serial.print(F("play NFC dir `"));
  Serial.print(queueDirname);
  Serial.println(F("`"));
#endif
#if SERIAL_DEBUG_VERBOSE && false
  printDirectory(SD.open(queueDirname));
#endif
}
