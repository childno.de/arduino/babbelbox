/*
 * get byte array as string
 * we use this to use NFC Tag ID as play folder name 
 * from https://stackoverflow.com/a/44749986/529977
 */
void array_to_string(byte a[], unsigned int len, char b[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (a[i] >> 4) & 0x0F;
        byte nib2 = (a[i] >> 0) & 0x0F;
        b[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        b[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    b[len*2] = '\0';
}

// from rfid_read_personal_data.ino example
String readMifareUid() {
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return String("");
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return String("");
  }
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
#if SERIAL_DEBUG
  mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); //dump some details about the card
#endif

  char uidstr[8] = "";
  array_to_string(mfrc522.uid.uidByte, 4, uidstr);
  return uidstr;
}
