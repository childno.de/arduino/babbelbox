// Buttons
#define BUTTON_STOP       A0
#define BUTTON_PLAY_PAUSE A2
#define BUTTON_SOFTER     A4
#define BUTTON_LOUDER     A5

boolean buttonReleased = true;

void setupButtons() {
  // alternative musicPlayer.GPIO_pinMode(i, INPUT);
  //             musicPlayer.GPIO_digitalRead(i, HIGH);
  pinMode(BUTTON_PLAY_PAUSE, INPUT_PULLUP);
  pinMode(BUTTON_STOP, INPUT_PULLUP);
  pinMode(BUTTON_SOFTER, INPUT_PULLUP);
  pinMode(BUTTON_LOUDER, INPUT_PULLUP);
}
