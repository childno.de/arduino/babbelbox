void playToneSequence(uint8_t e, uint8_t z, uint8_t d, uint16_t ms) {
  ampOn(true);
  delay(10);
  musicPlayer.sineTest(e, ms);
  musicPlayer.sineTest(z, ms);
  musicPlayer.sineTest(d, ms);
  delay(50);
  ampOn(false);
}

void playToneSequence(uint8_t e, uint8_t z, uint16_t ms) {
  ampOn(true);
  delay(10);
  musicPlayer.sineTest(e, ms);
  musicPlayer.sineTest(z, ms);
  delay(50);
  ampOn(false);
}

void playTone(uint8_t t, uint16_t ms) {
  ampOn(true);
  delay(10);
  musicPlayer.sineTest(t, ms);
  delay(50);
  ampOn(false);
}
