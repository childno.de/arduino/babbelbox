#include <SPI.h>
#include <MFRC522.h>

#define PIN_MFRC522_RESET 9      // Configurable, see typical pin layout above
#define PIN_MFRC522_SS    10     // Configurable, see typical pin layout above

MFRC522 mfrc522(PIN_MFRC522_SS, PIN_MFRC522_RESET);

void setupMifareBoard() {
  mfrc522.PCD_Init();   // Init MFRC522 module
}

/**
   from firmware_check.ino

   This is a MFRC522 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid

   This example test the firmware of your MFRC522 reader module, only known version can be checked. If the test passed
   it do not mean that your module is faultless! Some modules have bad or broken antennas or the PICC is broken.
   NOTE: for more informations read the README.rst

   @author Rotzbua
   @license Released into the public domain.
*/
bool checkMifareBoard() {
  while (!Serial);      // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();          // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522 module

  bool checkOk = mfrc522.PCD_PerformSelfTest();
#if SERIAL_DEBUG_VERBOSE
  Serial.println(F("== MFRC522 Digital self test"));
  mfrc522.PCD_DumpVersionToSerial();  // Show version of PCD - MFRC522 Card Reader
  Serial.print(F("Test Result: "));
  Serial.println(checkOk ? F("OK") : F("DEFECT or UNKNOWN"));
#endif
  return checkOk;
}
