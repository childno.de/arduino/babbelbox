// no more needed since ???
// #include <Arduino.h>
/*
   Current (simple) Design
   - 4 Buttons (louder, softer, pause, stop)
   - NFC Reader unwritten, NUID is used as folder name
   - mono amp out due to amp hardware and space box limitations

   Note on power supply
   - please do empower with only USB, see https://learn.adafruit.com/ladyadas-learn-arduino-lesson-number-0/power-jack-and-supply

   used libraries
   - Adafruit_VS1053
   - MFRC522
   - SD
   - SPI

   unused libraries
   - https://github.com/miguelbalboa/rfid
*/

#define SERIAL_DEBUG          false
#define SERIAL_DEBUG_VERBOSE  false
#define COMMIT_SOUND          true

#define EXPERIMENTAL          false

// !! This sketch is splitted into different files; ordering matters and set by file name alphanum sort
//01babbelBox_amp.ino
//01babbelBox_buttons.ino
//01babbelBox_rc522.ino
//01babbelBox_rc522_util.ino
//01babbelBox_SD.ino
//01babbelBox_SD_util.ino
//01babbelBox_vs1053.ino
//01babbelBox_vs1053_util.ino
//02babbelBox_setup.ino
//03babbelBox_loop_rc522.ino
//04babbelBox_loop.ino

/**
   Changelog:
    - sineTest tone on startup and shutdown
    - forward / rewind per file, still no fast forward in file
*/

/**
   Problems:
    (/) no new NFC Tag is accepted once playlist is done
    - file sort is non-deterministic / by inode not alphanum as "expected
        - We should have a look at new SDfat http://forum.arduino.cc/index.php?topic=259317.0
    - initial volume is not applied
    - AAC/mp4 get stuck sometimes?
      - underpowered? sometime resumes when plugging in / off _additional_ USB to arduino board aside 9V/1A jacked power supply
      - needs patch for vs1053b?
*/

/**
   TODO: (aside inline TODOs)
    (/) stop playlist if new tag is detected
    - use buttons instead of serial in
      - https://arduino.stackexchange.com/questions/10081/
    (*) play folder
      - see link above, just read a list and play
        - question SD read files list ordered, perhaps by reading mp3 id track number? https://arduino.stackexchange.com/questions/10081/
      (*) auto play next file in folder
    - write current file for …
    - … resume on next folder play
    - restart folder by long pause or stop
    - fast forward / speed WRAMADDR 0x1E04
      - see also jumpPoints SCI_HDAT1 0x1e16-25
    - m3u playing
    - disable amp as long DREQ is down instead of defined code points
    - piezo summer for error codes with e.g. Tones library to get hints with hardware defects
    - startup admin phase
      - allow 1 second to press buttons
        - e.g. all buttons: sineTest babbelBox Code Version
          . . . - . . . - i.e. 3xshort, 1 long 2x looped i.e. binary 0001 0001 i.e. 1.1
    - patch https://github.com/TheNitek/RfidShelf/blob/master/RfidShelf/RfidShelf.ino
      - https://mpflaga.github.io/Arduino_Library-vs1053_for_SdFat/
      - http://www.vlsi.fi/en/support/software.html
      - http://www.vlsi.fi/fileadmin/software/VS10XX/vs1053b-patches.pdf
    - mono by SW
      - https://github.com/TheNitek/RfidShelf/blob/master/RfidShelf/RfidShelf.ino#L126
        musicPlayer.sciWrite(VS1053_REG_WRAMADDR, 0x1e09);
        musicPlayer.sciWrite(VS1053_REG_WRAM, 0x0001);
        - seems to be wrong, see https://cdn-shop.adafruit.com/datasheets/vs1053.pdf
          9.6.6    SCI_AUDATA (RW)
    - mono by hardware e.g. https://store.arduino.cc/adafruit-i2s-3w-class-d-amplifier-breakout-max98357a
    - Stereo Support
    - headphone optional jack
      - see https://electronics.stackexchange.com/questions/307687 or https://electronics.stackexchange.com/questions/95575
    - lookup what great ideas others do have
      - https://www.voss.earth/tonuino/
        - https://github.com/xfjx/TonUINO
      - patched fork https://github.com/seisfeld/TonUINO
      - https://github.com/TheNitek/RfidShelf
      - https://chrigas.blogspot.com/2014/10/arduino-mp3-player-1-auswahl-der.html?m=1
      - https://github.com/paulgreg/arduino_mp3_player with OLED
    - auto power off in idle mode and power on at any keypress

   NICE TO HAVE
    - eInk display
    - bluetooth smartphone parent configurator
      - map folders / track by smartphone to a given NFC Card
    - bluetooth smartphone input
*/
